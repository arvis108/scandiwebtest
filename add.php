<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles/styles.css">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        function fetch_select(val) {
            $.ajax({
                type: 'post',
                url: 'includes/form_validation.php',
                data: {
                    get_option: val
                },
                success: function(response) {
                    document.getElementById("new_div").innerHTML = response;
                }
            });
        }
        $(function() {
            $('form').on('submit', function(e) {
                e.preventDefault();
                $.ajax({
                    type: 'post',
                    url: 'includes/form_validation.php',
                    data: $('form').serialize(),
                    success: function(response) {
                        if (response == true) {
                            window.location.href = "index.php";
                        } else {
                            document.getElementById("error").innerHTML = response;
                        }
                    }
                });
            });
        });
    </script>
</head>

<body>
    <header>
        <h1>Product Add</h1>
        <div id="button-box">
            <input type="submit" name="submit" value="Save" form="myform">
            <a href="index.php">Cancel</a>
        </div>
    </header>
    <div class="line"></div>
    <form class="form" id="myform">
        <div id="form-box">
            <div id="error"></div>
            <div class="field">
                <label for="SKU">SKU</label>
                <input type="text" name="Sku" id="SKU">
            </div>
            <div class="field">
                <label for="name">Name</label>
                <input type="text" name="Name" id="name">
            </div>
            <div class="field">
                <label for="price">Price($)</label>
                <input type="text" name="Price" id="price">
            </div>
            <div class="field">
                <label for="switch">Type Switcher</label>
                <select name="obj" id="switch" onchange="fetch_select(this.value);">
                    <option value="def">Type Switcher</option>
                    <option value="discClass">DVD</option>
                    <option value="bookClass">Book</option>
                    <option value="furnitureClass">Furniture</option>
                </select>
            </div>
            <div id="new_div">
            </div>

        </div>
    </form>
    <div class="line"></div>
    <footer>
        <h4>Scandiweb Test assignment</h4>
    </footer>
</body>


</html>