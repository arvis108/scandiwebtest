<?php
require_once 'includes/dbcontroller.php';
$db_handle = new DBController();
$conn = $db_handle->getConn();
if (isset($_POST['delete']) && isset($_POST["ids"])) {
    foreach ($_POST["ids"] as $id) {
        if (is_numeric($id)) {
            $sql = "DELETE FROM products WHERE ID={$id}";
            mysqli_query($conn, $sql);
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles/styles.css">
    <title>Document</title>
</head>

<body>
    <header>
        <h1>Product List</h1>
        <div id="button-box">
            <a href="add.php">ADD</a>
            <form action="index.php" method="POST">
                <input type="submit" name="delete" value="MASS DELETE">
        </div>
    </header>
    <div class="line"></div>
    <div id="product-grid">
        <?php
        $product_array = $db_handle->getDBResult("SELECT * FROM products;");
        if (!empty($product_array)) {
            foreach ($product_array as $key => $value) {
        ?>
                <div class="product-item">
                    <input type="checkbox" class="check" name='ids[]' value=<?php echo $product_array[$key]["ID"]; ?>></input>
                    <div class="product-values">
                        <div class="product-SKU"><?php echo $product_array[$key]["SKU"]; ?></div>
                        <div class="product-name"><?php echo $product_array[$key]["Name"]; ?></div>
                        <div class="product-price"><?php echo $product_array[$key]["Price"] . "$"; ?></div>
                        <div class="product-atribute"><?php echo $product_array[$key]["Atribute"] . " " . $product_array[$key]["Value"]; ?></div>
                    </div>

                </div>
        <?php
            }
        }
        ?>
    </div>
    </form>
    <div class="line"></div>
    <footer>
        <h4>Scandiweb Test assignment</h4>
    </footer>
</body>

</html>