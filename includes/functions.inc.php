<?php

function emptyInput($sku, $name, $price, $type, ...$params)
{
    $result = false;
    if (empty($sku) || empty($name) || empty($price) || empty($type)) {
        $result = true;
    }
    if (count($params) != 0) {
        foreach ($params as $n) {
            if (empty($n)) {
                $result = true;
            }
        }
    }
    return $result;
}
