<?php

namespace includes;

require_once 'mainProductClass.php';
require_once 'dbcontroller.php';
class bookClass extends Product
{
    private $weight;
    private $atribute = 'Weight';

    public function MyConstruct($sku, $name, $price, $weight = null)
    {
        parent::MyConstruct($sku, $name, $price);
        if (empty($weight)) {
            echo 'Fill all fields';
            exit();
        } elseif (!is_numeric($weight)) {
            echo 'Size should be numeric value';
            exit();
        } else {
            $this->weight = $weight;
        }
    }
    public function getAtribute()
    {
        return $this->atribute;
    }

    public function getWeight()
    {
        return $this->weight;
    }
    public function setWeight($weight)
    {
        if (is_numeric($weight) && !empty($weight)) {
            $this->weight = $weight;
        } else {
            echo 'Weight should be numeric value';
            exit();
        }
    }

    public function addProduct($conn)
    {
        $sql = "INSERT INTO products (SKU,Name,Price,Atribute,Value) VALUES (?,?,?,?,?)";
        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("location: ../add.php?error=stmtfailed");
            exit();
        }
        $sku = parent::getSku();
        $name = parent::getName();
        $price = parent::getPrice();
        $endValue = $this->weight . 'KG';
        mysqli_stmt_bind_param($stmt, "ssdss", $sku, $name, $price, $this->atribute, $endValue);
        mysqli_stmt_execute($stmt);

        echo true;
    }
}
