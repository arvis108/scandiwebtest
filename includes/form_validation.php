<?php
require_once 'functions.inc.php';
require_once 'dbcontroller.php';
require_once 'mainProductClass.php';
require_once 'bookClass.php';
require_once 'discClass.php';
require_once 'furnitureClass.php';


$db_handle = new DBController();
$conn = $db_handle->getConn();

// class bookClass extends includes\bookClass // includes\bookClass is loaded here
// {
// }

//dynamic form
if (isset($_POST['get_option'])) {
    switch ($_POST['get_option']) {
        case 'discClass':
            echo "<div class='field'> <label for='size'>Size (MB)</label>
            <input type='text' name='Size' id='size'></div>
            <p class='msg'>Please provide dvd size in MB, only number needed</p>";
            break;
        case 'bookClass':
            echo "<div class='field'><label for='weight'>Weight (KG)</label>
            <input type='text' name='Weight' id='weight'></div>
            <p class='msg'>Please provide weight of the book in KG,only number needed</p>";
            break;
        case 'furnitureClass':
            echo "<div class='field'> <label for='height'>Height (CM)</label>
            <input type='text' name='Height' id='height'></div>
            <div class='field'> <label for='width'>Width (CM)</label>
            <input type='text' name='Width' id='width'></div>
            <div class='field'> <label for='length'>Length (CM)</label>
            <input type='text' name='Length' id='length'></div>
            <p class='msg'>Please provide dimensions in HxWxL format</p>";
            break;
    }
    exit();
}
if (!emptyInput($_POST['Sku'], $_POST['Name'], $_POST['Price'], $_POST['obj'])) {

    $str = $_POST['obj']; //class name
    //if is selected one of product types, depending on the choose class object will be created
    if ($str != 'def') {
        $class = 'includes\\' . $str;
        $obj = new $class;
        //specified object class setters will be called using data from POST
        //after all values are set - object is initialized
        foreach ($_POST as $key => $value) {
            if ($key == 'obj') {
                continue;
            }
            $set = 'set' . $key;
            $obj->$set($value);
        }
        //sending data to database
        $obj->addProduct($conn);
    } else {
        echo 'Type is not selected';
        exit();
    }
} else {
    echo 'Fill all fields';
}
