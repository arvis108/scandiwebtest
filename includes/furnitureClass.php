<?php

namespace includes;

require_once 'mainProductClass.php';
class furnitureClass extends Product
{
    private $Height;
    private $Width;
    private $Length;
    private $atribute = 'Dimension';
    public function MyConstruct($sku, $name, $price, $Height = null, $Width = null, $Length = null)
    {
        parent::MyConstruct($sku, $name, $price);
        if (empty($Height) || empty($Width) || empty($Length)) {
            echo 'Fill all fields';
            exit();
        } elseif (!is_numeric($Height) || !is_numeric($Width) || !is_numeric($Length)) {
            echo 'Height,Width and Length value should be numeric';
            exit();
        } else {
            $this->Height = $Height;
            $this->Width = $Width;
            $this->Length = $Length;
        }
    }

    public function getHeight()
    {
        return $this->Height;
    }
    public function setHeight($Height)
    {
        if (is_numeric($Height) && !empty($Height)) {
            $this->Height = $Height;
        } else {
            echo 'Height should be numeric';
            exit();
        }
    }
    public function getWidth()
    {
        return $this->Width;
    }
    public function setWidth($Width)
    {
        if (is_numeric($Width) && !empty($Width)) {
            $this->Width = $Width;
        } else {
            echo 'Width should be numeric';
            exit();
        }
    }
    public function getLength()
    {
        return $this->Length;
    }
    public function setLength($Length)
    {
        if (is_numeric($Length) && !empty($Length)) {
            $this->Length = $Length;
        } else {
            echo 'Length should be numeric';
            exit();
        }
    }

    public function addProduct($conn)
    {
        $sql = "INSERT INTO products (SKU,Name,Price,Atribute,Value) VALUES (?,?,?,?,?)";
        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("location: ../add.php?error=stmtfailed");
            exit();
        }
        $sku = parent::getSku();
        $name = parent::getName();
        $price = parent::getPrice();
        $endValue = $this->Height . 'x' . $this->Width . 'x' . $this->Length;
        mysqli_stmt_bind_param($stmt, "ssdss", $sku, $name, $price, $this->atribute, $endValue);
        mysqli_stmt_execute($stmt);

        echo true;
    }
}
