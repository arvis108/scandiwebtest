<?php
class DBController
{
    private $host = "localhost";
    private $user = "root";
    private $password = "root";
    private $database = "uzdevums";
    private $port = 3307;
    private $conn;

    function __construct()
    {
        $this->conn = mysqli_connect($this->host, $this->user, $this->password, $this->database, $this->port);
    }
    function getConn()
    {
        return  $this->conn;
    }
    function getDBResult($query)
    {
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $resultset[] = $row;
        }

        if (!empty($resultset)) {
            return $resultset;
        }
    }
}
