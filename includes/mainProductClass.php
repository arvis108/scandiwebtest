<?php

namespace includes;

abstract class Product
{
    private $sku;
    private $name;
    private $price;

    public function MyConstruct($sku, $name, $price)
    {
        if (empty($sku) || empty($name) || empty($price)) {
            echo 'Fill all fields';
            exit();
        } elseif (!is_numeric($price)) {
            echo 'Bad price value';
            exit();
        } elseif (!preg_match("/^[a-zA-Z0-9]*$/", $sku) || strlen($sku) < 7) {
            echo 'Bad SKU value';
            exit();
        } else {
            $this->sku = $sku;
            $this->name = htmlspecialchars($name);
            $this->price = $price;
        }
    }

    public function getSku()
    {
        return $this->sku;
    }
    public function setSku($sku)
    {
        if (empty($sku) || (!preg_match("/^[a-zA-Z0-9]*$/", $sku) && strlen($sku) < 7)) {
            echo 'Bad SKU value';
            exit();
        } else {
            $this->sku = $sku;
        }
    }

    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        if (empty($name)) {
            echo 'Empty name value';
            exit();
        } else {
            $this->name = htmlspecialchars($name);
        }
    }

    public function getPrice()
    {
        return $this->price;
    }
    public function setPrice($price)
    {
        if (!is_numeric($price)) {
            echo 'Price should be numeric value';
            exit();
        } else {
            $this->price = $price;
        }
    }
}
