<?php

namespace includes;

require_once 'mainProductClass.php';
class discClass extends Product
{
    private $size;
    private $atribute = 'Size';
    public function MyConstruct($sku, $name, $price, $size = null)
    {
        parent::MyConstruct($sku, $name, $price);
        if (empty($size)) {
            echo 'Fill all fields';
            exit();
        } elseif (!is_numeric($size)) {
            echo 'Size should be numeric value';
            exit();
        } else {
            $this->size = $size;
        }
    }
    public function getSize()
    {
        return $this->size;
    }
    public function setSize($size)
    {
        if (is_numeric($size) && !empty($size)) {
            $this->size = $size;
        } else {
            echo 'Size should be numeric value';
            exit();
        }
    }

    public function addProduct($conn)
    {
        $sql = "INSERT INTO products (SKU,Name,Price,Atribute,Value) VALUES (?,?,?,?,?)";
        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("location: ../add.php?error=stmtfailed");
            exit();
        }
        $sku = parent::getSku();
        $name = parent::getName();
        $price = parent::getPrice();
        $endValue = $this->size . 'MB';
        mysqli_stmt_bind_param($stmt, "ssdss", $sku, $name, $price, $this->atribute, $endValue);
        mysqli_stmt_execute($stmt);

        echo true;
    }
}
