-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3307
-- Generation Time: Jan 20, 2021 at 07:34 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.1
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */
;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */
;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */
;
/*!40101 SET NAMES utf8mb4 */
;
--
-- Database: `uzdevums`
--
-- --------------------------------------------------------
--
-- Table structure for table `products`
--
CREATE TABLE `products` (
  `ID` int(11) NOT NULL,
  `SKU` varchar(15) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Price` double NOT NULL,
  `Atribute` varchar(20) NOT NULL,
  `Value` varchar(30) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
--
-- Dumping data for table `products`
--
INSERT INTO `products` (
    `ID`,
    `SKU`,
    `Name`,
    `Price`,
    `Atribute`,
    `Value`
  )
VALUES (1, 'JVC200123', 'Acme DISC', 1, 'Size', '700MB'),
  (
    2,
    'TR120555',
    'Chair',
    40,
    'Dimension',
    '24x45x15'
  ),
  (
    3,
    'GGWP0007',
    'War and Peace',
    20,
    'Weight',
    '2KG'
  ),
  (
    11,
    'JVC200666',
    'Roses Are Red',
    13.54,
    'Weight',
    '2.43KG'
  ),
  (
    12,
    'JVC200669',
    'Table',
    87.99,
    'Dimension',
    '150x200x120'
  ),
  (14, 'JVC200664', 'DVD', 4.56, 'Size', '1024MB'),
  (15, 'JVC200123', 'Acme DISC', 1, 'Size', '700MB'),
  (16, 'JVC200123', 'Acme DISC', 1, 'Size', '700MB'),
  (17, 'JVC200123', 'Acme DISC', 1, 'Size', '700MB'),
  (
    18,
    'TR120555',
    'Chair',
    40,
    'Dimension',
    '24x45x15'
  ),
  (
    19,
    'TR120555',
    'Chair',
    40,
    'Dimension',
    '24x45x15'
  ),
  (
    20,
    'TR120555',
    'Chair',
    40,
    'Dimension',
    '24x45x15'
  ),
  (
    21,
    'GGWP0007',
    'War and Peace',
    20,
    'Weight',
    '2KG'
  ),
  (
    22,
    'GGWP0007',
    'War and Peace',
    20,
    'Weight',
    '2KG'
  ),
  (
    23,
    'GGWP0007',
    'War and Peace',
    20,
    'Weight',
    '2KG'
  );
--
-- Indexes for dumped tables
--
--
-- Indexes for table `products`
--
ALTER TABLE `products`
ADD PRIMARY KEY (`ID`);
--
-- AUTO_INCREMENT for dumped tables
--
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 24;
COMMIT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */
;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */
;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */
;